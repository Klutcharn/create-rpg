﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_create
{
    public class Mage : Character
    {

        public Mage()
        {
            /// <summary>
            ///  Creating a hero with inherited properties from Character with stats and equipment allowments according to the assignment
            /// </summary>
            Strength = 1;
            Dexterity = 1;
            Intelligence = 8;
            Weapons = new List<Weaponkind> { Weaponkind.Staff, Weaponkind.Wand};
            Armors = new List<ArmorType> {ArmorType.Cloth };
        }

        public override void LevelUp()
        {
            /// <summary>
            ///  Levels the hero and adds the stats according to the assignment
            /// </summary>
            Level++;
            Strength++;
            Dexterity++;
            Intelligence += 5;
        }
        public override void FindPrimary()
        {
            /// <summary>
            ///  Setting primary attribute to the one according to the assignment
            /// </summary>
            PrimAttribute = Intelligence;
        }
    }
}
