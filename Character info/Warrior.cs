﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_create
{
    public class Warrior : Character
    {

        public Warrior()
        {
            /// <summary>
            ///  Creating a hero with inherited properties from Character with stats and equipment allowments according to the assignment
            /// </summary>
            Strength = 5;
            Dexterity = 2;
            Intelligence = 1;
            Weapons = new List<Weaponkind> { Weaponkind.Axe, Weaponkind.Hammer, Weaponkind.Sword };
            Armors = new List<ArmorType> { ArmorType.Plate, ArmorType.Mail };
        }

        public override void LevelUp()
        {
            /// <summary>
            ///  Levels the hero and adds the stats according to the assignment
            /// </summary>
            Level++;
            Strength += 3;
            Dexterity += 2;
            Intelligence++;
        }

        public override void FindPrimary()
        {
            /// <summary>
            ///  Setting primary attribute to the one according to the assignment
            /// </summary>
            PrimAttribute = Strength;
            
        }
    }
}
