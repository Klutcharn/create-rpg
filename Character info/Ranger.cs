﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_create
{
    public class Ranger : Character
    {

        public Ranger()
        {
            /// <summary>
            ///  Creating a hero with inherited properties from Character with stats and equipment allowments according to the assignment
            /// </summary>
            Strength = 1;
            Dexterity = 7;
            Intelligence = 1;
            Weapons = new List<Weaponkind> { Weaponkind.Bow };
            Armors = new List<ArmorType> { ArmorType.Leather, ArmorType.Mail };
        }

        public override void LevelUp()
        {
            /// <summary>
            ///  Levels the hero and adds the stats according to the assignment
            /// </summary>
            Level++;
            Strength++;
            Dexterity += 5;
            Intelligence++;
        }
        public override void FindPrimary()
        {
            /// <summary>
            ///  Setting primary attribute to the one according to the assignment
            /// </summary>
            PrimAttribute = Dexterity;
        }
    }
}
