﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_create
{
    public abstract class Character
    {
        /// <summary>
        /// Creating a base class for character creation, this has some abstract methods, some base stats and all kinds of properties to be used from all rpg-classes that inherit this.
        /// </summary>
        public string Name;
        public int Level;
        public int Strength;
        public int Dexterity;
        public int Intelligence;
        public double Damage;
        public int[] attributes = new int[] {};
        public float PrimAttribute;
        public List <Weaponkind> Weapons = new List<Weaponkind>();
        public List <ArmorType> Armors = new List<ArmorType>(); 
        public Weapon currentWeapon;
        public Armor currentHelm;
        public Armor currentChest;
        public Armor currentPants;
        public abstract void LevelUp();
        public abstract void FindPrimary();
        public Character()
        {
            Level = 1;
            attributes = new int[] {Strength,Dexterity,Intelligence };
            PrimAttribute = attributes.Max();
            currentWeapon = new Weapon("None",12312312, 1, new WeaponAttributes(0,1));
            currentHelm = new Armor("None", 1, 1, new ArmorAttributes(0, 0, 0));
            currentChest = new Armor("None", 1, 1, new ArmorAttributes(0, 0, 0));
            currentPants = new Armor("None", 1, 1, new ArmorAttributes(0, 0, 0));
            Damage = 1 + currentWeapon.DamageFromWeap;}
            
        }
        
        
    
}

