﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_create
{
    public enum Weaponkind
    {
        /// <summary>
        ///Lists all different kinds of weapons in an enum list
        /// </summary>
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand
    }
    public enum ArmorType
    {
        /// <summary>
        ///Lists all different kinds of armortypes in an enum list
        /// </summary>
        Cloth,
        Leather,
        Mail,
        Plate
    }
    public enum ItemPlace
    {
        /// <summary>
        ///Lists all different kinds of itemplaces in an enum list
        /// </summary>
        Head,
        Chest,
        Pants,
        Weapon
    }


}
