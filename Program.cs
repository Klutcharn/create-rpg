﻿
namespace RPG_create
{
    class main
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("Create hero for your adventure!");
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("Press 1 to select Warrior");
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("Press 2 to select Mage");
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("Press 3 to select Rogue");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Press 4 to select Ranger");
            String choise = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.White;

            bool correctChoice = false;
            Character Char = new Warrior();

            while (!correctChoice)
            {
                /// <summary>
                /// Creating a hero based on the choices you select, warrior, mage.
                /// If the choice is not valid u get prompted to select again
                /// </summary>
                switch (choise)
                {
                    case "1":
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("You chose: warrior");
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine("Enter your name");
                        String userName = Console.ReadLine();
                        Char = new Warrior()
                        {
                            Name = userName,
                        };
                        correctChoice = true;
                        break;
                    case "2":
                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        Console.WriteLine("You chose: Mage");
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine("Enter your name");
                        userName = Console.ReadLine();
                        Char = new Mage()
                        {
                            Name = userName,
                        };
                        correctChoice = true;
                        break;
                    case "3":
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.WriteLine("You chose: Rogue");
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine("Enter your name");
                        userName = Console.ReadLine();
                        Char = new Rogue()
                        {
                            Name = userName,
                        };
                        correctChoice = true; break;
                    case "4":
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                        Console.WriteLine("You chose: Ranger");
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine("Enter your name");
                        userName = Console.ReadLine();
                        Char = new Ranger()
                        {
                            Name = userName,
                        };
                        correctChoice = true; break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Magenta;
                        Console.WriteLine("Create hero for your adventure!");
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("Press 1 to select Warrior");
                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        Console.WriteLine("Press 2 to select Mage");
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.WriteLine("Press 3 to select Rogue");
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                        Console.WriteLine("Press 4 to select Ranger");
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                }
                if (!correctChoice) { choise = Console.ReadLine(); }
            }
            //if equiped weapon dmg below otherwise 1
            Char.Damage = 1;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Name:" + Char.Name + " Dexterity:" + Char.Dexterity + " Strength:" + Char.Strength + " Intelligence:" + Char.Intelligence + " Damage:" + Math.Round(Char.Damage, 1) + " Level:" + Char.Level);
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("Main menu");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Level up or equip items?");
            Console.WriteLine("1 to level up, 2 to equip item, 3 to show items and stats");
            String option = Console.ReadLine();
            bool correctOption = false;
            string choice = "";
            int chosenWeapId = 0;
            while (!correctOption)
            {
                /// <summary>
                /// Making the user select between leveling, equiping items or showing stats
                /// If the choice is not valid u get prompted to select again
                /// </summary>
                switch (option)
                {
                    case "1":
                        /// <summary>
                        /// Leveling up
                        /// </summary>
                        Char.LevelUp();
                        Console.WriteLine("Your current level is: " + Char.Level);
                        correctOption = true;
                        break;
                    case "2":
                        /// <summary>
                        /// Prompting the user to select between weapons,armors and going back.
                        /// </summary>
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.WriteLine("Equipment Page.");
                        correctOption = true;
                        bool shop = true;
                        while (shop)
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("1.Weapons");
                            Console.WriteLine("2.Armor");
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.WriteLine("3.Return to leveling");
                            string shopChoice = Console.ReadLine();
                            if (shopChoice == "1")
                            {
                                /// <summary>
                                ///If selection is weapons
                                ///All weapong are printed out
                                ///You can then choose the weapon and it then checks whether that hero can equip it or not.
                                /// </summary>
                                WeaponItems._item.ForEach(item => { Console.WriteLine(item.Id + " Name:" + item.ItemName + "\t DPS:" + Math.Round(item.DamageFromWeap, 0) + "\t Type:" + item.Type + "\t Level:" + item.ItemLevel); });
                                Console.WriteLine("Write a number to equip or type anything else to go back to the equip page.");
                                choice = Console.ReadLine();
                                bool isNumber = int.TryParse(choice, out _);
                                if (isNumber)
                                {
                                    chosenWeapId = Int32.Parse(choice);
                                    if (chosenWeapId <= WeaponItems._item.Count())
                                    {
                                        Weapon selectedWeap = WeaponItems._item[chosenWeapId - 1];
                                        Methods.CheckWeapon(Char, selectedWeap);
                                    }
                                    else
                                    {
                                        /// <summary>
                                        ///If you select a weapon that isnt in the list you get sent back and prompted to select a new existing weapon.
                                        /// </summary>
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.WriteLine("You want to equip an item that doesnt exists?!");
                                        Console.WriteLine("Write a number that we have in stock next time!");
                                        Console.ForegroundColor = ConsoleColor.White;
                                    }
                                }
                                else
                                /// <summary>
                                ///If you select a letter instead of a number you get prompted to choose a number instead.
                                /// </summary>
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Enter a valid number");
                                    Console.ForegroundColor = ConsoleColor.White;
                                }
                            }
                            else if (shopChoice == "2")
                            /// <summary>
                            ///If selection is Armors
                            ///All armors are printed out
                            ///You can then choose the armors and it then checks whether that hero can equip it or not.
                            /// </summary>
                            {
                                ArmorItems._item.ForEach(item => { Console.WriteLine(item.Id + " Name:" + item.ItemName + "\t Type:" + item.Type + "\t Place:" + item.Place + "\t Level:" + item.ItemLevel); });
                                Console.WriteLine("Write a number to equip or type anything else to go back to the equip page.");
                                choice = Console.ReadLine();
                                bool isNumber = int.TryParse(choice, out _);
                                if (isNumber)
                                {
                                    chosenWeapId = Int32.Parse(choice);
                                    if (chosenWeapId <= ArmorItems._item.Count())
                                    {
                                        Armor selectedArmor = ArmorItems._item[chosenWeapId - 1];
                                        Methods.CheckArmor(Char, selectedArmor);
                                        /// <summary>
                                        ///Shows which gear is equiped.
                                        /// </summary>
                                        Console.WriteLine("Equiped helm: " + Char.currentHelm.ItemName);
                                        Console.WriteLine("Equiped chest: " + Char.currentChest.ItemName);
                                        Console.WriteLine("Equiped pants: " + Char.currentPants.ItemName);
                                        Console.WriteLine("Equiped weapon: " + Char.currentWeapon.ItemName);
                                    }
                                    else
                                    /// <summary>
                                    ///If you try to equip a number that doesnt exist you get prompted to select an item that does exist.
                                    /// </summary>
                                    {
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.WriteLine("You want to equip an item that doesnt exists?!");
                                        Console.WriteLine("Write a number that we have in stock next time!");
                                        Console.ForegroundColor = ConsoleColor.White;
                                    }
                                }
                                else
                                /// <summary>
                                ///If you try to enter something taht isnt a number you get prompted to select a valid number.
                                /// </summary>
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Enter a valid number next time.");
                                    Console.ForegroundColor = ConsoleColor.White;
                                }
                            }
                            else if (shopChoice == "3")
                            /// <summary>
                            ///If you want to go back you get sent back.
                            /// </summary>
                            {
                                shop = false;
                            }
                            else
                            /// <summary>
                            ///if not selecting weapon, armor or go back you get prompted to make a valid choice.
                            /// </summary>
                            {
                                Console.WriteLine("Select again");
                            }
                        }
                        break;
                    case "3":
                        /// <summary>
                        ///Shows all current stats, equipment and also does a damage calculation from primary stats.
                        /// </summary>
                        correctOption = true;
                        Console.WriteLine("Current stats");
                        Console.ForegroundColor = ConsoleColor.Green;
                        Char.FindPrimary();
                        if (Char.currentWeapon.DamageFromWeap > 1)
                        /// <summary>
                        ///If you have a weapon equiped you get one type of calcultion else you get one without a weapon.
                        /// </summary>
                        {
                            Char.Damage = Char.currentWeapon.DamageFromWeap * (1 + (Char.PrimAttribute / 100));
                        }
                        else
                        {
                            Char.Damage = (1 + (Char.PrimAttribute / 100));
                        }
                        Console.WriteLine("Name:" + Char.Name + " Level:" + Char.Level + " Dexterity:" + Char.Dexterity + " Strength:" + Char.Strength + " Intelligence:" + Char.Intelligence + " Damage:" + Math.Round(Char.Damage, 1));
                        Console.WriteLine("Equiped helm:" + Char.currentHelm.ItemName);
                        Console.WriteLine("Equiped chest:" + Char.currentChest.ItemName);
                        Console.WriteLine("Equiped pants:" + Char.currentPants.ItemName);
                        Console.WriteLine("Equiped weapon:" + Char.currentWeapon.ItemName);
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        break;
                    default:
                        /// <summary>
                        ///If you dont enter a valid choice you get pronpted to make another choice
                        /// </summary>
                        Console.WriteLine("Please enter a valid value.");
                        Console.WriteLine("Press 1 to level up");
                        Console.WriteLine("Press 2 to equip items");
                        Console.WriteLine("Press 3 to view stats and items");
                        break;
                }
                if (!correctOption)
                /// <summary>
                ///If choice is not valid from menu to chose level,equip or show stats you get prompted again.
                /// </summary>
                {
                    option = Console.ReadLine();
                }
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Level some or go back");
                Console.WriteLine(" 1 to go level more, anything else to go back to main menu");
                Console.ForegroundColor = ConsoleColor.White;
                bool opt = true;
                option = Console.ReadLine();
                while (opt)
                /// <summary>
                ///If you want to level some more or go back to main menu
                /// </summary>
                {
                    switch (option)
                    {
                        case "1":
                            opt = false;
                            correctOption = false;
                            break;
                        default:
                            opt = false;
                            correctOption = false;
                            Console.ForegroundColor = ConsoleColor.Magenta;
                            Console.WriteLine("Main menu");
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.WriteLine("Level up or equip items?");
                            Console.WriteLine("1 to level up, 2 to equip item, 3 to view stats and items");
                            option = Console.ReadLine();
                            break;
                    }
                }
                if (opt)
                {
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    Console.WriteLine("Main menu");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("Level up or equip items?");
                    Console.WriteLine("1 to level up, 2 to equip item, 3 to view stats and items");
                    option = Console.ReadLine();
                    correctOption = false;
                }
            }
        }
    }
}