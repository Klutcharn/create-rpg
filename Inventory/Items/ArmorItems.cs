﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_create
{
    public static class ArmorItems
    {
        public static List<Armor> _item;
        /// <summary>
        /// Creating a list full of different armors with all kinds of stats, types, places and levels
        /// </summary>
        static ArmorItems()
        {
            _item = new List<Armor>();
            _item.Add(new Armor("Hood of haduken",1,1, new ArmorAttributes(0, 0, 3))
            {
                Type = ArmorType.Cloth,
                Place = ItemPlace.Head,
            });
            _item.Add(new Armor("Chest of Time",2,3, new ArmorAttributes(0, 0, 7))
            {
                Type = ArmorType.Cloth,
                Place = ItemPlace.Chest,
             });
            _item.Add(new Armor("Pantaloons",3,2, new ArmorAttributes(0, 0, 5))
            {
                Type = ArmorType.Cloth,
                Place = ItemPlace.Pants,
            });
            _item.Add(new Armor("Dark goggles",4,1, new ArmorAttributes(0, 3, 0))
            {
                Type = ArmorType.Leather,
                Place = ItemPlace.Head,
            });
            _item.Add(new Armor("Sneaky doublet ",5,6, new ArmorAttributes(0, 7, 0))
            {
                Type = ArmorType.Leather,
                Place = ItemPlace.Chest,
            });
            _item.Add(new Armor("Leather chaps",6,3, new ArmorAttributes(0, 5, 0))
            {
                Type = ArmorType.Leather,
                Place = ItemPlace.Pants,
            });
            _item.Add(new Armor("Chainmail cowl", 7, 2, new ArmorAttributes(0, 3, 0))
            {
                Type = ArmorType.Mail,
                Place = ItemPlace.Head,
            });
            _item.Add(new Armor("Hunters jacket ", 8, 4, new ArmorAttributes(0, 7, 0))
            {
                Type = ArmorType.Mail,
                Place = ItemPlace.Chest,
            });
            _item.Add(new Armor("Silver Breeches", 9, 5, new ArmorAttributes(0, 5, 0))
            {
                Type = ArmorType.Mail,
                Place = ItemPlace.Pants,
            });
            _item.Add(new Armor("Belzebubs horns", 10, 2, new ArmorAttributes(3, 0, 0))
            {
                Type = ArmorType.Plate,
                Place = ItemPlace.Head,
            });
            _item.Add(new Armor("Plate cuirass", 11, 5, new ArmorAttributes(7,0, 0))
            {
                Type = ArmorType.Plate,
                Place = ItemPlace.Chest,
            });
            _item.Add(new Armor("Shiny cuisses", 12, 3, new ArmorAttributes(5,0, 0))
            {
                Type = ArmorType.Plate,
                Place = ItemPlace.Pants,
            });
        }
    }
}
