﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_create
{
    public class WeaponItems
    {
        public static List<Weapon> _item;

        /// <summary>
        /// Creating a list full of different weapons with all kinds of stats, types and levels
        /// </summary>
        static WeaponItems()
        {
            _item = new List<Weapon>();
            _item.Add(new Weapon("Staff of thumas", 1, 1, new WeaponAttributes(15, 0.8) { })
            {
                Type = Weaponkind.Staff,
                Place = ItemPlace.Weapon
            }); ;
            _item.Add(new Weapon("Axe of Hell", 2, 1, new WeaponAttributes(60, 0.3))
            {
                Type = Weaponkind.Axe,
                Place = ItemPlace.Weapon
            });
            _item.Add(new Weapon("Wooden Sword", 3, 2, new WeaponAttributes(5, 2))
            {
                Type = Weaponkind.Sword,
                Place = ItemPlace.Weapon,
            });
            _item.Add(new Weapon("Stabby stabsters", 4, 1, new WeaponAttributes(8, 1.7))
            {
                Type = Weaponkind.Dagger,
                Place = ItemPlace.Weapon,
            });
            _item.Add(new Weapon("Smashy smasher", 5, 4, new WeaponAttributes(45, 0.5))
            {
                Type = Weaponkind.Hammer,
                Place = ItemPlace.Weapon,
            });
            _item.Add(new Weapon("Demon bow", 6, 1, new WeaponAttributes(25, 1))
            {
                Type = Weaponkind.Bow,
                Place = ItemPlace.Weapon,
            });
            _item.Add(new Weapon("Rainbow wand", 7, 1, new WeaponAttributes(17, 1))
            {
                Type = Weaponkind.Wand,
                Place = ItemPlace.Weapon,
            });
            _item.Add(new Weapon("Bigboi Axe", 8, 5, new WeaponAttributes(80, 0.6))
            {
                Type = Weaponkind.Axe,
                Place = ItemPlace.Weapon,
            });
        }
    }
}
