﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_create
{
    public class WeaponAttributes
    {
        /// <summary>
        ///a class to make weapon attributes such as damage and attackspeed per weapon
        /// </summary>
        public int Damage { get; set; }
        public double AttackSpeed { get; set; }
        public double DPS { get; set; }
        public WeaponAttributes(int dmg, double atkSpd)
        {
            Damage = dmg;
            AttackSpeed = atkSpd;
            DPS = atkSpd*dmg;
        }
    }
    public class Weapon
    {
        /// <summary>
        ///a class to make weapons that is used when making different weapons later on in the program.
        /// </summary>
        public readonly string ItemName = "";
        public readonly int ItemLevel = 0;
        public readonly int Id;
        public readonly double DamageFromWeap;
        public Weaponkind Type;
        public ItemPlace Place;
        public WeaponAttributes Weap { get; set; }

        public Weapon(string name,int id,  int level, WeaponAttributes weap)
        {
            ItemName = name;
            Id = id;
            ItemLevel = level;
            Weap = weap;
            DamageFromWeap = weap.AttackSpeed * weap.Damage;
            Type = Weaponkind.Bow;
            Place = ItemPlace.Weapon;
        }
    }
}
