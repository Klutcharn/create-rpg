﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_create
{

    public class ArmorAttributes
    {
        /// <summary>
        /// Creating attribute class that can be added to armor pieces
        /// </summary>
        public int Strength;
        public int Dexterity;
        public int Intelligence;

        public double AttackSpeed;
        public ArmorAttributes(int str, int dex, int inte)
        {
            Strength = str;
            Dexterity = dex;
            Intelligence = inte;
        }
    }

    public class Armor
    {
        /// <summary>
        /// Creating a class for armors, this will then be applied to the creation of the armor pieces.
        /// </summary>
        public readonly string ItemName = "";
        public readonly int ItemLevel = 0;
        public ArmorType Type;
        public ItemPlace Place;
        public readonly int Id;
        public ArmorAttributes Attributes;
        public Armor(string name, int id, int level, ArmorAttributes attributes)
        {
            Id = id;
            ItemName = name;
            ItemLevel = level;
            Type = ArmorType.Cloth;
            Place = ItemPlace.Head;
            Attributes = attributes;

        }
    }


}

