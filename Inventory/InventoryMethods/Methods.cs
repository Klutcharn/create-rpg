﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_create
{
    public class Methods
    {
        public static void RemoveStats(Character hero, Armor current)
        {
            /// <summary>
            /// Removing the stats from the current equiped amrmor
            /// </summary>
            hero.Strength -= current.Attributes.Strength;
            hero.Dexterity -= current.Attributes.Dexterity;
            hero.Intelligence -= current.Attributes.Intelligence;
            
        }

        public static void UpdateStats(Character hero, Armor current)
        {
            /// <summary>
            /// Updating the stats from the current equiped amrmor
            /// </summary>
            hero.Strength += current.Attributes.Strength;
            hero.Dexterity += current.Attributes.Dexterity;
            hero.Intelligence += current.Attributes.Intelligence;
           
        }


        public static void CheckWeapon(Character hero, Weapon weapon)
        {
            Weapon selectedWeap = weapon;

            try { 
            if (hero.Weapons.Contains(selectedWeap.Type) && selectedWeap.ItemLevel <= hero.Level)
            /// <summary>
            ///Checks whether your hero can equip that type of weapon and checks the weapons level and compares it to yours.
            /// </summary>
            {
                if (hero.currentWeapon.DamageFromWeap > 1)
                /// <summary>
                ///If you already have a weapon it then unequips the weapon and equips the new one
                ///If you dont have a weapon equiped it just equips the one you wanted.
                /// </summary>
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Removed: " + hero.currentWeapon.ItemName);
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Equiped: " + selectedWeap.ItemName);
                    hero.currentWeapon = selectedWeap;
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Equiped: " + selectedWeap.ItemName);
                    hero.currentWeapon = selectedWeap;
                    Console.ForegroundColor = ConsoleColor.White;
                }
            }
            else
            /// <summary>
            ///This throws different exceptions depending on what kind of rule is broking in the equipment stage
            /// </summary>
            {
                if (!hero.Weapons.Contains(selectedWeap.Type))
                {
                    InvalidWeaponTypeException ex = new InvalidWeaponTypeException();
                    Console.ForegroundColor = ConsoleColor.Red;
                    throw new InvalidWeaponTypeException();
                    Console.WriteLine(ex.Message);
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    InvalidWeaponLevelException ex = new InvalidWeaponLevelException();
                    Console.ForegroundColor = ConsoleColor.Red;
                    throw new InvalidWeaponLevelException();
                    Console.WriteLine(ex.Message);
                    Console.ForegroundColor = ConsoleColor.White;
                }
            }
            }
            catch (InvalidWeaponLevelException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (InvalidWeaponTypeException ex)
            {
                Console.WriteLine(ex.Message);
            }
         
        }
        public static void CheckArmor(Character hero, Armor armor)
        {
            Armor selectedArmor = armor;
            try { 
            if (hero.Armors.Contains(selectedArmor.Type) && selectedArmor.ItemLevel <= hero.Level)
            /// <summary>
            ///Checks whether you can equip that type of armor and if your hero is high enought level.
            /// </summary>
            {
                /// <summary>
                ///Below it does checks on what place the item you selected belongs and equips it in the correct place.
                /// </summary>
                if (selectedArmor.Place == ItemPlace.Head)
                {
                    if (hero.currentHelm.ItemName != "None")
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Removed: " + hero.currentHelm.ItemName);
                        Methods.RemoveStats(hero, hero.currentHelm);

                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Equiped: " + selectedArmor.ItemName);
                        Console.ForegroundColor = ConsoleColor.White;
                        hero.currentHelm = selectedArmor;
                        Methods.UpdateStats(hero, hero.currentHelm);
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Equiped: " + selectedArmor.ItemName);
                        Console.ForegroundColor = ConsoleColor.White;
                        hero.currentHelm = selectedArmor;
                        Methods.UpdateStats(hero, hero.currentHelm);
                    }
                }
                else if (selectedArmor.Place == ItemPlace.Chest)
                {
                    if (hero.currentChest.ItemName != "None")
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Removed: " + hero.currentChest.ItemName);
                        Methods.RemoveStats(hero, hero.currentHelm);
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Equiped: " + selectedArmor.ItemName);
                        Console.ForegroundColor = ConsoleColor.White;
                        hero.currentChest = selectedArmor;
                        Methods.UpdateStats(hero, hero.currentChest);
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Equiped: " + selectedArmor.ItemName);
                        Console.ForegroundColor = ConsoleColor.White;
                        hero.currentChest = selectedArmor;
                        Methods.UpdateStats(hero, hero.currentChest);
                    }
                }
                else if (selectedArmor.Place == ItemPlace.Pants)
                {
                    if (hero.currentPants.ItemName != "None")
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Removed: " + hero.currentPants.ItemName);
                        Methods.RemoveStats(hero, hero.currentHelm);
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Equiped: " + selectedArmor.ItemName);
                        Console.ForegroundColor = ConsoleColor.White;
                        hero.currentPants = selectedArmor;
                        Methods.UpdateStats(hero, hero.currentPants);
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Equiped: " + selectedArmor.ItemName);
                        Console.ForegroundColor = ConsoleColor.White;
                        hero.currentPants = selectedArmor;
                        Methods.UpdateStats(hero, hero.currentPants);
                    }
                }
            }
            else
            /// <summary>
            ///Depening on what rule was not fulfilled it then throws and exception
            /// </summary>
            {
                if (!hero.Armors.Contains(selectedArmor.Type))
                {
                    InvalidArmorTypeException ex = new InvalidArmorTypeException();
                    Console.ForegroundColor = ConsoleColor.Red;
                    throw new InvalidArmorTypeException();

                    Console.ForegroundColor = ConsoleColor.White;
                }
                else if (selectedArmor.ItemLevel > hero.Level)
                {
                    InvalidArmorLevelException ex = new InvalidArmorLevelException();
                    Console.ForegroundColor = ConsoleColor.Red;
                    throw new InvalidArmorLevelException();

                    Console.ForegroundColor = ConsoleColor.White;
                }
            }
            } catch (InvalidArmorLevelException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (InvalidArmorTypeException ex)
            {
                Console.WriteLine(ex.Message);
            }


        }
    }


    public class InvalidWeaponLevelException : Exception
    {
        /// <summary>
        /// Custom exception for weapon
        /// </summary>
        public InvalidWeaponLevelException()
          : base(String.Format("Invalid Weapon, level too high"))
        { }
    }

    public class InvalidWeaponTypeException : Exception
    {
        /// <summary>
        /// Custom exception for weapon
        /// </summary>
        public InvalidWeaponTypeException()
          : base(String.Format("Invalid Weapon, Wrong type"))
        { }
    }
    public class InvalidArmorLevelException : Exception
    {
        /// <summary>
        /// Custom exception for armor
        /// </summary>
        public InvalidArmorLevelException()
          : base(String.Format("Invalid Armor, level too high"))
        { }
    }

    public class InvalidArmorTypeException : Exception
    {
        /// <summary>
        /// Custom exception for armor
        /// </summary>
        public InvalidArmorTypeException()
          : base(String.Format("Invalid Armor, Wrong type"))
        { }
    }
}


